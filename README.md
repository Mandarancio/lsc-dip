# LSC and DIP

**Author**: Martino Ferrari   
**Contact**: manda.mgf@gmail.com

## Notebooks

Two example notebooks can be found in the folder `dip` and `lsc`.

## Execute

To execute `lsc` or `dip` simply execute the following:
```
python3 dip conf.yml
```
or 
```
python3 lsc conf.yml
```

## Configurations

Some configuration examples are stored in `lsc_conf` and `dip_conf`.